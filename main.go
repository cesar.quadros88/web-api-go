package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"goweb/config"
	"goweb/handlers"
	"net/http"
)

func main() {
	err := config.Load()
	if err != nil {
		panic(err)
	}
	r := chi.NewRouter()
	r.Post("/", handlers.Create)
	r.Put("/{id}", handlers.Update)
	r.Get("/", handlers.List)
	r.Get("/{id}", handlers.Get)

	http.ListenAndServe(fmt.Sprintf(":%s", config.GetServerPort()), r)
}
