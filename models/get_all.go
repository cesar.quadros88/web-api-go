package models

import (
	"goweb/db"
	"log"
)

func GetAll() (todos []Todo, err error) {
	conn, err := db.OpenConnection()
	if err != nil {
		return
	}
	defer conn.Close()
	rows, err := conn.Query(`SELECT * FROM todos`)
	if err != nil {
		log.Print("Erro getAll")
		return
	}
	for rows.Next() {
		log.Print("Pegando um todo")
		var todo Todo
		err = rows.Scan(&todo.ID, &todo.Name, &todo.Description, &todo.Done)
		if err != nil {
			continue
		}
		todos = append(todos, todo)
	}
	return
}
