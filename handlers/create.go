package handlers

import (
	"encoding/json"
	"goweb/models"
	"log"
	"net/http"
)

func Create(w http.ResponseWriter, r *http.Request) {
	var todo models.Todo
	err := json.NewDecoder(r.Body).Decode(&todo)
	if err != nil {
		log.Printf("Error decoding json: %v", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	id, err := models.Insert(todo)

	var resp map[string]interface{}
	if err != nil {
		resp = map[string]interface{}{
			"Error":   true,
			"message": "Error creating todo"}
	} else {
		resp = map[string]interface{}{
			"Error":   false,
			"message": "Todo created successfully",
			"id":      id}
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}
